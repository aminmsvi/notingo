package com.github.aminmsvi.notingo;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.main.MainActivity;
import com.github.aminmsvi.notingo.ui.notepad.NotePadActivity;

/**
 * Created by aminmsvi on 8/31/2017
 */
public class Router {

	private Router() {
	}

	public static void navigateToMainActivity(@NonNull Context context) {
		Intent intent = MainActivity.getCallingIntent(context);
		context.startActivity(intent);
	}

	public static void navigateToNotePadActivityForResult(@NonNull Fragment fragment, int requestCode, @NonNull Note note) {
		Intent intent = NotePadActivity.getCallingIntent(fragment.getContext(), note);
		fragment.startActivityForResult(intent, requestCode);
	}
}
