package com.github.aminmsvi.notingo.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Directory;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryDao;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryRepository;
import com.github.aminmsvi.notingo.data.source.note.NoteDao;

/**
 * Created by aminmsvi on 8/25/2017
 */
@Database(entities = {Directory.class, Note.class},
		version = 1)
public abstract class NotingoDatabase extends RoomDatabase {

	private static final Object sLock = new Object();
	private static NotingoDatabase sInstance;

	public static NotingoDatabase getInstance(@NonNull Context context) {
		synchronized (sLock) {
			if (sInstance == null) {
				sInstance = Room.databaseBuilder(context.getApplicationContext(), NotingoDatabase.class, "notingo.db")
						.addCallback(new OnDatabaseCreate())
						.build();
			}
			return sInstance;
		}
	}

	public abstract DirectoryDao directoryDao();

	public abstract NoteDao noteDao();

	private static class OnDatabaseCreate extends Callback {

		@Override
		public void onCreate(@NonNull SupportSQLiteDatabase db) {
			super.onCreate(db);
			ContentValues cv = new ContentValues();
			Directory root = new Directory(DirectoryRepository.DIRECTORY_ROOT_ID,
			                               DirectoryRepository.DIRECTORY_ROOT_ID,
			                               null);
			cv.put("id", root.getId());
			cv.put("name", root.getName());
			cv.put("parent_directory_id", root.getParentDirectoryId());
			db.insert("directory", SQLiteDatabase.CONFLICT_REPLACE, cv);
		}
	}
}
