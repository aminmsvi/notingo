package com.github.aminmsvi.notingo.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

/**
 * Created by aminmsvi on 8/25/2017
 */
@Entity(tableName = "directory")
public class Directory {

	@NonNull
	@PrimaryKey
	@ColumnInfo(name = "id")
	private final String mId;

	@NonNull
	@ColumnInfo(name = "name")
	private final String mName;

	@Nullable
	@ColumnInfo(name = "parent_directory_id")
	private final String mParentDirectoryId;

	@Ignore
	public Directory(@NonNull String name, @Nullable String parentDirectoryId) {
		this(UUID.randomUUID().toString(), name, parentDirectoryId);
	}

	public Directory(@NonNull String id, @NonNull String name, @Nullable String parentDirectoryId) {
		mId = id;
		mName = name;
		mParentDirectoryId = parentDirectoryId;
	}

	@NonNull
	public String getId() {
		return mId;
	}

	@NonNull
	public String getName() {
		return mName;
	}

	@Nullable
	public String getParentDirectoryId() {
		return mParentDirectoryId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Directory directory = (Directory) o;

		return mId.equals(directory.mId) &&
				mName.equals(directory.mName);

	}

	@Override
	public int hashCode() {
		int result = mId.hashCode();
		result = 31 * result + mName.hashCode();
		return result;
	}
}
