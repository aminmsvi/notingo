package com.github.aminmsvi.notingo.data.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.source.directory.DirectoryRepository;

import java.util.UUID;

/**
 * Created by aminmsvi on 8/25/2017
 */
@SuppressWarnings("unused")
@Entity(tableName = "note",
		indices = @Index(value = "directory_id"),
		foreignKeys = @ForeignKey(entity = Directory.class,
				parentColumns = "id",
				childColumns = "directory_id",
				onDelete = ForeignKey.CASCADE,
				onUpdate = ForeignKey.CASCADE))
public class Note implements Parcelable {

	public static final short NOTE_TYPE_TEXT = 0x0;
	public static final short NOTE_TYPE_PAINTING = 0x1;

	public static final Creator<Note> CREATOR = new Creator<Note>() {
		@Override
		public Note createFromParcel(Parcel in) {
			return new Note(in);
		}

		@Override
		public Note[] newArray(int size) {
			return new Note[size];
		}
	};

	@NonNull
	@PrimaryKey
	@ColumnInfo(name = "id")
	private final String mId;
	@ColumnInfo(name = "type")
	private short mType;
	@NonNull
	@ColumnInfo(name = "directory_id")
	private String mDirectoryId;
	@NonNull
	@ColumnInfo(name = "title")
	private String mTitle;
	@NonNull
	@ColumnInfo(name = "body")
	private String mBody;
	@NonNull
	@ColumnInfo(name = "painting_path")
	private String mPaintingPath;

	/**
	 * Used for parsing from xml. Currently only text notes can be parsed from xml!
	 */
	@Ignore
	public Note() {
		mId = UUID.randomUUID().toString();
		mType = NOTE_TYPE_TEXT;
		mDirectoryId = DirectoryRepository.DIRECTORY_ROOT_ID;
		mTitle = "";
		mBody = "";
		mPaintingPath = "";
	}

	@Ignore
	public Note(@NonNull String directoryId, @NonNull String paintingPath) {
		this(directoryId, "", "", paintingPath, NOTE_TYPE_PAINTING);
	}

	@Ignore
	public Note(@NonNull String directoryId, @NonNull String title, @NonNull String body) {
		this(directoryId, title, body, "", NOTE_TYPE_TEXT);
	}

	@Ignore
	public Note(@NonNull String directoryId, @NonNull String title, @NonNull String body, @NonNull String paintingPath, short type) {
		this(UUID.randomUUID().toString(), directoryId, title, body, paintingPath, type);
	}

	public Note(@NonNull String id, @NonNull String directoryId, @NonNull String title, @NonNull String body, @NonNull String paintingPath, short type) {
		mId = id;
		mDirectoryId = directoryId;
		mTitle = title;
		mBody = body;
		mPaintingPath = paintingPath;
		mType = type;
	}

	protected Note(Parcel in) {
		mId = in.readString();
		mDirectoryId = in.readString();
		mTitle = in.readString();
		mBody = in.readString();
		mPaintingPath = in.readString();
		mType = (short) in.readInt();
	}

	@NonNull
	public String getId() {
		return mId;
	}

	@NonNull
	public String getDirectoryId() {
		return mDirectoryId;
	}

	public void setDirectoryId(@NonNull String directoryId) {
		mDirectoryId = directoryId;
	}

	@NonNull
	public String getTitle() {
		return mTitle;
	}

	public void setTitle(@NonNull String title) {
		mTitle = title;
	}

	@NonNull
	public String getBody() {
		return mBody;
	}

	public void setBody(@NonNull String body) {
		mBody = body;
	}

	@NonNull
	public String getPaintingPath() {
		return mPaintingPath;
	}

	public void setPaintingPath(@NonNull String paintingPath) {
		mPaintingPath = paintingPath;
	}

	public short getType() {
		return mType;
	}

	public void setType(short type) {
		mType = type;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mId);
		dest.writeString(mDirectoryId);
		dest.writeString(mTitle);
		dest.writeString(mBody);
		dest.writeString(mPaintingPath);
		dest.writeInt(mType);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@SuppressWarnings("SimplifiableIfStatement")
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Note note = (Note) o;

		if (mType != note.mType) return false;
		if (!mId.equals(note.mId)) return false;
		if (!mDirectoryId.equals(note.mDirectoryId)) return false;
		if (!mTitle.equals(note.mTitle)) return false;
		if (!mBody.equals(note.mBody)) return false;
		return mPaintingPath.equals(note.mPaintingPath);

	}

	@Override
	public int hashCode() {
		int result = mId.hashCode();
		result = 31 * result + mDirectoryId.hashCode();
		result = 31 * result + mTitle.hashCode();
		result = 31 * result + mBody.hashCode();
		result = 31 * result + mPaintingPath.hashCode();
		result = 31 * result + (int) mType;
		return result;
	}
}
