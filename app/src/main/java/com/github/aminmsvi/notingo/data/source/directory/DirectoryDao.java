package com.github.aminmsvi.notingo.data.source.directory;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.data.entity.Directory;

import java.util.List;

/**
 * Created by aminmsvi on 8/25/2017
 */
@Dao
public interface DirectoryDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insertDirectories(Directory... directories);

	@Update
	int updateDirectories(Directory... directories);

	@Delete
	void deleteDirectories(Directory... directories);

	@Query("SELECT * FROM directory")
	List<Directory> getDirectories();

	@Query("SELECT * FROM directory WHERE parent_directory_id = :parentDirectoryId")
	List<Directory> getDirectoriesWithParentId(@Nullable String parentDirectoryId);

	@Nullable
	@Query("SELECT * FROM directory WHERE id = :directoryId")
	Directory getDirectoryById(@Nullable String directoryId);
}
