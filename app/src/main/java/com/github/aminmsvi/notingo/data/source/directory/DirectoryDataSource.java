package com.github.aminmsvi.notingo.data.source.directory;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.data.entity.Directory;

import java.util.List;

/**
 * Created by aminmsvi on 8/25/2017
 */
public interface DirectoryDataSource {

	void getDirectories(@NonNull ListCallback callback);

	void getDirectoriesWithParentId(@Nullable String parentDirectoryId, @NonNull ListCallback callback);

	void saveDirectories(Directory... directories);

	void updateDirectories(Directory... directories);

	void deleteDirectories(Directory... directories);

	void getDirectoryById(@Nullable String directoryId, @NonNull SingleCallback callback);

	interface ListCallback {

		void onDirectoriesLoaded(@NonNull List<Directory> directories);

		void onNoDataAvailable();
	}

	interface SingleCallback {

		void onDirectoryLoaded(@NonNull Directory directory);

		void onNoDataAvailable();
	}
}
