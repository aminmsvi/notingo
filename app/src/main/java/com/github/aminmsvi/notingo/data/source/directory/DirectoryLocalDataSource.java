package com.github.aminmsvi.notingo.data.source.directory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.data.NotingoDatabase;
import com.github.aminmsvi.notingo.data.entity.Directory;
import com.github.aminmsvi.notingo.executor.AppExecutors;

import java.util.List;

/**
 * Created by aminmsvi on 8/26/2017
 */
class DirectoryLocalDataSource implements DirectoryDataSource {

	private final Context mContext;
	private final AppExecutors mAppExecutors;

	DirectoryLocalDataSource(Context context, AppExecutors appExecutors) {
		mContext = context;
		mAppExecutors = appExecutors;
	}

	@Override
	public void saveDirectories(final Directory... directories) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.insertDirectories(directories);
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void updateDirectories(final Directory... directories) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.updateDirectories(directories);
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void deleteDirectories(final Directory... directories) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.deleteDirectories(directories);
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void getDirectoryById(@Nullable final String directoryId, @NonNull final SingleCallback callback) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				final Directory directory = NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.getDirectoryById(directoryId);

				mAppExecutors.getMainThread().execute(new Runnable() {
					@Override
					public void run() {
						if (directory != null) {
							callback.onDirectoryLoaded(directory);
						} else {
							callback.onNoDataAvailable();
						}
					}
				});
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void getDirectories(@NonNull final ListCallback callback) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				final List<Directory> directories = NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.getDirectories();

				mAppExecutors.getMainThread().execute(new Runnable() {
					@Override
					public void run() {
						if (!directories.isEmpty()) {
							callback.onNoDataAvailable();
						} else {
							callback.onDirectoriesLoaded(directories);
						}
					}
				});
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void getDirectoriesWithParentId(@Nullable final String parentDirectoryId, @NonNull final ListCallback callback) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				final List<Directory> directories = NotingoDatabase.getInstance(mContext)
						.directoryDao()
						.getDirectoriesWithParentId(parentDirectoryId);

				mAppExecutors.getMainThread().execute(new Runnable() {
					@Override
					public void run() {
						if (directories.isEmpty()) {
							callback.onNoDataAvailable();
						} else {
							callback.onDirectoriesLoaded(directories);
						}
					}
				});
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}
}
