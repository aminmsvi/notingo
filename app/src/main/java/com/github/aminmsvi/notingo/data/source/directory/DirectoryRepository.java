package com.github.aminmsvi.notingo.data.source.directory;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.data.entity.Directory;
import com.github.aminmsvi.notingo.executor.AppExecutors;

/**
 * Created by aminmsvi on 8/26/2017
 */
public class DirectoryRepository implements DirectoryDataSource {

	public static final String DIRECTORY_ROOT_ID = "root";

	private static DirectoryRepository sInstance;

	private final DirectoryDataSource mLocalDataSource;

	private DirectoryRepository(DirectoryDataSource localDataSource) {
		mLocalDataSource = localDataSource;
	}

	public static DirectoryRepository getInstance(@NonNull Context context, @NonNull AppExecutors appExecutor) {
		if (sInstance == null) {
			sInstance = new DirectoryRepository(new DirectoryLocalDataSource(context, appExecutor));
		}
		return sInstance;
	}

	@Override
	public void getDirectories(@NonNull ListCallback callback) {
		mLocalDataSource.getDirectories(callback);
	}

	@Override
	public void getDirectoriesWithParentId(@Nullable String parentDirectoryId, @NonNull ListCallback callback) {
		mLocalDataSource.getDirectoriesWithParentId(parentDirectoryId, callback);
	}

	@Override
	public void saveDirectories(@NonNull Directory... directories) {
		mLocalDataSource.saveDirectories(directories);
	}

	@Override
	public void updateDirectories(@NonNull Directory... directories) {
		mLocalDataSource.updateDirectories(directories);
	}

	@Override
	public void deleteDirectories(@NonNull Directory... directories) {
		mLocalDataSource.deleteDirectories(directories);
	}

	@Override
	public void getDirectoryById(@Nullable String directoryId, @NonNull SingleCallback callback) {
		mLocalDataSource.getDirectoryById(directoryId, callback);
	}
}
