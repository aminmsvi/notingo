package com.github.aminmsvi.notingo.data.source.note;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Note;

import java.util.List;

/**
 * Created by aminmsvi on 8/25/2017
 */
@Dao
public interface NoteDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	void insertNotes(Note... notes);

	@Delete
	void deleteNotes(Note... notes);

	@Query("SELECT * FROM note WHERE directory_id = :directoryId")
	List<Note> getNotesWithDirectoryId(@NonNull String directoryId);

	@Query("SELECT * FROM note WHERE id = :noteId")
	Note getNoteWithId(@NonNull String noteId);
}
