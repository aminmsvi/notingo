package com.github.aminmsvi.notingo.data.source.note;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Note;

import java.util.List;

/**
 * Created by aminmsvi on 8/26/2017
 */
public interface NoteDataSource {

	void getNotesWithDirectoryId(@NonNull String directoryId, @NonNull ListCallback callback);

	void getNoteWithId(@NonNull String noteId, @NonNull SingleCallback callback);

	void saveNotes(Note... notes);

	void deleteNotes(Note... notes);

	interface ListCallback {

		void onNotesLoaded(@NonNull List<Note> notes);

		void onNoDataAvailable();
	}

	interface SingleCallback {

		void onNoteLoaded(@NonNull Note note);

		void onNoDataAvailable();
	}
}
