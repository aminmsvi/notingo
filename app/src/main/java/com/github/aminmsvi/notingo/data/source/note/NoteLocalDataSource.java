package com.github.aminmsvi.notingo.data.source.note;

import android.content.Context;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.NotingoDatabase;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.executor.AppExecutors;

import java.util.List;

/**
 * Created by aminmsvi on 8/26/2017
 */
class NoteLocalDataSource implements NoteDataSource {

	private final Context mContext;
	private final AppExecutors mAppExecutors;

	NoteLocalDataSource(Context context, AppExecutors appExecutors) {
		mContext = context;
		mAppExecutors = appExecutors;
	}

	@Override
	public void getNotesWithDirectoryId(@NonNull final String directoryId, @NonNull final ListCallback callback) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				final List<Note> notes = NotingoDatabase.getInstance(mContext)
						.noteDao()
						.getNotesWithDirectoryId(directoryId);
				mAppExecutors.getMainThread().execute(new Runnable() {
					@Override
					public void run() {
						if (!notes.isEmpty()) {
							callback.onNotesLoaded(notes);
						} else {
							callback.onNoDataAvailable();
						}
					}
				});
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void getNoteWithId(@NonNull final String noteId, @NonNull final SingleCallback callback) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				final Note note = NotingoDatabase.getInstance(mContext)
						.noteDao()
						.getNoteWithId(noteId);
				mAppExecutors.getMainThread().execute(new Runnable() {
					@Override
					public void run() {
						if (note != null) {
							callback.onNoteLoaded(note);
						} else {
							callback.onNoDataAvailable();
						}
					}
				});
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);

	}

	@Override
	public void saveNotes(final Note... notes) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				NotingoDatabase.getInstance(mContext)
						.noteDao()
						.insertNotes(notes);
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}

	@Override
	public void deleteNotes(final Note... notes) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				NotingoDatabase.getInstance(mContext)
						.noteDao()
						.deleteNotes(notes);
			}
		};
		mAppExecutors.getDiskIO().execute(runnable);
	}
}
