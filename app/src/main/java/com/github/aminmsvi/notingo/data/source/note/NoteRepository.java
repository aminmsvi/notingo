package com.github.aminmsvi.notingo.data.source.note;

import android.content.Context;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.executor.AppExecutors;

/**
 * Created by aminmsvi on 8/26/2017
 */
public class NoteRepository implements NoteDataSource {

	private static NoteRepository sInstance;

	private final NoteLocalDataSource mLocalDataSource;

	private NoteRepository(NoteLocalDataSource localDataSource) {
		mLocalDataSource = localDataSource;
	}

	public static NoteRepository getInstance(@NonNull Context context, @NonNull AppExecutors appExecutor) {
		if (sInstance == null) {
			sInstance = new NoteRepository(new NoteLocalDataSource(context, appExecutor));
		}
		return sInstance;
	}

	@Override
	public void getNotesWithDirectoryId(@NonNull String directoryId, @NonNull ListCallback callback) {
		mLocalDataSource.getNotesWithDirectoryId(directoryId, callback);
	}

	@Override
	public void getNoteWithId(@NonNull String noteId, @NonNull SingleCallback callback) {
		mLocalDataSource.getNoteWithId(noteId, callback);
	}

	@Override
	public void saveNotes(Note... notes) {
		mLocalDataSource.saveNotes(notes);
	}

	@Override
	public void deleteNotes(Note... notes) {
		mLocalDataSource.deleteNotes(notes);
	}
}
