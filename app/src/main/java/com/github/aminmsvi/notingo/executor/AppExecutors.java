package com.github.aminmsvi.notingo.executor;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

/**
 * Created by aminmsvi on 8/29/2017
 */
public class AppExecutors {

	private static AppExecutors sInstance;

	private final Executor mDiskIO;
	private final Executor mMainThread;

	private AppExecutors() {
		mDiskIO = new DiskIOThreadExecutor();
		mMainThread = new MainThreadExecutor();
	}

	public static AppExecutors getInstance() {
		if (sInstance == null) {
			sInstance = new AppExecutors();
		}
		return sInstance;
	}

	public Executor getDiskIO() {
		return mDiskIO;
	}

	public Executor getMainThread() {
		return mMainThread;
	}

	private static class MainThreadExecutor implements Executor {
		private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

		@Override
		public void execute(@NonNull Runnable command) {
			mainThreadHandler.post(command);
		}
	}
}
