package com.github.aminmsvi.notingo.executor;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by aminmsvi on 8/29/2017
 */
class DiskIOThreadExecutor implements Executor {

	private final Executor mDiskIO;

	DiskIOThreadExecutor() {
		mDiskIO = Executors.newSingleThreadExecutor();
	}

	@Override
	public void execute(@NonNull Runnable command) {
		mDiskIO.execute(command);
	}
}
