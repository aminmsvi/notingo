package com.github.aminmsvi.notingo.helper;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by aminmsvi on 9/1/2017
 */
public class PathHelper {

	private final Context mContext;

	public PathHelper(Context context) {
		mContext = context;
	}

	public File getFileDir() {
		return mContext.getFilesDir();
	}

	public File getExternalStorageDirectory() {
		return Environment.getExternalStorageDirectory();
	}

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
	}
}
