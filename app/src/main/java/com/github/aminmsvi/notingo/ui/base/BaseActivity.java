package com.github.aminmsvi.notingo.ui.base;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aminmsvi.notingo.R;

/**
 * Created by aminmsvi on 8/25/2017
 */
public abstract class BaseActivity extends AppCompatActivity implements MvpView {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onDestroy() {
		hideLoading();
		super.onDestroy();
	}

	@Override
	public void showLoading() {
	}

	@Override
	public void hideLoading() {
	}

	@Override
	public void showMessage(@StringRes int resId) {
		Toast toast = new Toast(this);
		toast.setView(LayoutInflater.from(this)
				              .inflate(R.layout.layout_toast, (ViewGroup) findViewById(android.R.id.content), false));
		TextView tvMessage = toast.getView().findViewById(R.id.tv_toast_message);
		tvMessage.setText(resId);
		toast.show();
	}

	@Override
	public void showError(@StringRes int resId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.all_error);
		builder.setMessage(resId);
		builder.setNeutralButton(R.string.all_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}

	@Override
	public void finish() {
		super.finish();
	}

	@TargetApi(Build.VERSION_CODES.M)
	public void requestPermissionsSafely(String[] permissions, int requestCode) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			requestPermissions(permissions, requestCode);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	public boolean hasPermission(String permission) {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
				checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
	}
}
