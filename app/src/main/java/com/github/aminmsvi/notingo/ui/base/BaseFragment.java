package com.github.aminmsvi.notingo.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;

/**
 * Created by aminmsvi on 8/29/2017
 */
public abstract class BaseFragment extends DialogFragment implements MvpView {

	private BaseActivity mBaseActivity;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof BaseActivity) {
			mBaseActivity = (BaseActivity) context;
		}
	}

	@Override
	public void onDetach() {
		mBaseActivity = null;
		super.onDetach();
	}

	@Override
	public void showLoading() {
		if (mBaseActivity != null) {
			mBaseActivity.showLoading();
		}
	}

	@Override
	public void showMessage(@StringRes int resId) {
		if (mBaseActivity != null) {
			mBaseActivity.showMessage(resId);
		}
	}

	@Override
	public void hideLoading() {
		if (mBaseActivity != null) {
			mBaseActivity.hideLoading();
		}
	}

	@Override
	public void showError(@StringRes int resId) {
		if (mBaseActivity != null) {
			mBaseActivity.showError(resId);
		}
	}

	@Override
	public void finish() {
		super.dismiss();
	}

	public BaseActivity getBaseActivity() {
		return mBaseActivity;
	}

	@TargetApi(Build.VERSION_CODES.M)
	public void requestPermissionsSafely(String[] permissions, int requestCode) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			requestPermissions(permissions, requestCode);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	public boolean hasPermission(String permission) {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
				ContextCompat.checkSelfPermission(getContext(), permission) == PackageManager.PERMISSION_GRANTED;
	}
}
