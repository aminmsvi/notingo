package com.github.aminmsvi.notingo.ui.base;

/**
 * Created by aminmsvi on 8/25/2017
 *
 * Base class that implements the Presenter interface and provides a base implementation for
 * attach() and detach(). It also handles keeping a reference to the mvpView that
 * can be accessed from the children classes by calling getMvpView().
 */
public abstract class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

	private V mMvpView;

	public BasePresenter() {
	}

	@Override
	public void attach(V mvpView) {
		mMvpView = mvpView;
	}

	@Override
	public void detach() {
		mMvpView = null;
	}

	protected boolean isViewAttached() {
		return mMvpView != null;
	}

	protected V getMvpView() {
		return mMvpView;
	}

	protected void checkViewAttached() {
		if (!isViewAttached()) throw new MvpViewNotAttachedException();
	}

	public static class MvpViewNotAttachedException extends RuntimeException {
		public MvpViewNotAttachedException() {
			super("Please call Presenter.attach(MvpView) before" +
					      " requesting data to the Presenter");
		}
	}
}
