package com.github.aminmsvi.notingo.ui.base;

/**
 * Created by aminmsvi on 8/25/2017
 */
public interface MvpPresenter<V extends MvpView> {

	void attach(V mvpView);

	void detach();
}
