package com.github.aminmsvi.notingo.ui.base;

import android.support.annotation.StringRes;

/**
 * Created by aminmsvi on 8/25/2017
 *
 * Base interface that any class that wants to act as a View in the MVP (Model View Presenter)
 * pattern must implement. Generally this interface will be extended by a more specific interface
 * that then usually will be implemented by an Activity or Fragment.
 */
public interface MvpView {

	/**
	 * Show a progress dialog
	 */
	void showLoading();

	/**
	 * Hide progress dialog, if available
	 */
	void hideLoading();

	/**
	 * Show a message box with error message
	 *
	 * @param resId Resource id of error message
	 */
	void showError(@StringRes int resId);

	/**
	 * Show a snackbar with provided message
	 *
	 * @param resId Resource id of message
	 */
	void showMessage(@StringRes int resId);

	/**
	 * Finish/Dismiss current form
	 */
	void finish();
}
