package com.github.aminmsvi.notingo.ui.custom;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;

import com.github.aminmsvi.notingo.R;

/**
 * Created by aminmsvi on 8/29/2017
 */
@SuppressWarnings("unused")
public class FabMenu extends LinearLayout implements View.OnClickListener {

	private static final float FAB_ANIMATION_START_THRESHOLD = 0.7f; // value below 0.7f breaks the animation!
	private static final int ANIMATION_DURATION = 1000;
	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 10;

	private ValueAnimator mValueAnimator = new ValueAnimator();
	private State mState = State.CLOSE;
	private int mMenuItemBottomMargin;

	public FabMenu(Context context) {
		super(context);
		init();
	}

	public FabMenu(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FabMenu(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		setOrientation(VERTICAL);
		setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
	}

	@Override
	public void onClick(View v) {
		switch (mState) {
			case OPEN:
				closeMenu(true);
				break;
			case CLOSE:
				openMenu(true);
				break;
		}
	}

	public void addMenuItem(@DrawableRes int resId, OnClickListener onClickListener) {
		FloatingActionButton fab = new FloatingActionButton(getContext());
		fab.setSize(FloatingActionButton.SIZE_MINI);
		fab.setImageResource(resId);
		fab.setOnClickListener(onClickListener);
		addView(fab, 0);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) fab.getLayoutParams();
		params.setMargins(0, mMenuItemBottomMargin, 0, 0);
		fab.setLayoutParams(params);

		if (mState == State.CLOSE) {
			fab.setAlpha(0.0f);
			fab.setScaleY(0.0f);
			fab.setScaleX(0.0f);
			fab.setVisibility(View.INVISIBLE);
		}
	}

	public void removeMenuItem(int index) {
		removeViewAt(index);
		invalidate();
	}

	private void init() {
		inflate(getContext(), R.layout.view_fab_menu, this);
		FloatingActionButton fabToggleItems = findViewById(R.id.fab_toggle);
		fabToggleItems.setOnClickListener(this);
		mMenuItemBottomMargin = (int) (getContext().getResources().getDisplayMetrics().density * 16);
	}

	private void openMenu(boolean animate) {
		mState = State.OPENING;
		if (animate) {
			startAnimation(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					int value = (int) animation.getAnimatedValue();
					for (int i = 0; i < getMenuItemCount(); ++i) {
						FloatingActionButton item = (FloatingActionButton) getChildAt(Math.abs(getMenuItemCount() - 1 - i));
						if (item.getScaleX() > 1.0f - FAB_ANIMATION_START_THRESHOLD) continue;
						if (value < MAX_VALUE / (getMenuItemCount() - i)) {
							item.show();
							if (i == getMenuItemCount() - 1) {
								animation.end();
							}
							break;
						}
					}
				}
			}, new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					mState = State.OPEN;
				}
			});
		} else {
			for (int i = 0; i < getMenuItemCount(); i++) {
				View menuItem = getChildAt(i);
				menuItem.setAlpha(1.0f);
				menuItem.setScaleY(1.0f);
				menuItem.setScaleX(1.0f);
				menuItem.setVisibility(View.VISIBLE);
			}
		}
	}

	private void closeMenu(boolean animate) {
		mState = State.CLOSING;
		if (animate) {
			startAnimation(new ValueAnimator.AnimatorUpdateListener() {
				@Override
				public void onAnimationUpdate(ValueAnimator animation) {
					int value = (int) animation.getAnimatedValue();
					for (int i = getMenuItemCount() - 1; i >= 0; i--) {
						FloatingActionButton item = (FloatingActionButton) getChildAt(Math.abs(getMenuItemCount() - 1 - i));
						if (item.getScaleX() < FAB_ANIMATION_START_THRESHOLD) continue;
						if (value < MAX_VALUE / (getMenuItemCount() - i)) {
							item.hide();
							if (i == 0) {
								animation.end();
							}
							break;
						}
					}
				}
			}, new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					super.onAnimationEnd(animation);
					mState = State.CLOSE;
				}
			});
		} else {
			for (int i = 0; i < getMenuItemCount(); i++) {
				View menuItem = getChildAt(i);
				menuItem.setAlpha(0.0f);
				menuItem.setScaleY(0.0f);
				menuItem.setScaleX(0.0f);
				menuItem.setVisibility(View.INVISIBLE);
			}
		}
	}

	private void startAnimation(ValueAnimator.AnimatorUpdateListener animatorUpdateListener, AnimatorListenerAdapter animatorListenerAdapter) {
		if (mValueAnimator != null && mValueAnimator.isRunning()) {
			mValueAnimator.cancel();
		}
		mValueAnimator = ValueAnimator.ofInt(MIN_VALUE, MAX_VALUE);
		mValueAnimator.setInterpolator(new LinearInterpolator());
		mValueAnimator.setDuration(ANIMATION_DURATION);
		mValueAnimator.removeAllUpdateListeners();
		mValueAnimator.addUpdateListener(animatorUpdateListener);
		mValueAnimator.addListener(animatorListenerAdapter);
		mValueAnimator.start();
	}

	/**
	 * Returns menu Items count which is equals to all children expect toggle button.
	 *
	 * @return Count of menu items in this view.
	 */
	private int getMenuItemCount() {
		return getChildCount() - 1;
	}

	private enum State {
		OPEN, CLOSE, OPENING, CLOSING
	}
}
