package com.github.aminmsvi.notingo.ui.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by aminmsvi on 9/1/2017
 */
public class PaintingView extends View {

	private final static int DEFAULT_COLOR = Color.BLACK;
	private final static int DEFAULT_STROKE_WIDTH = 5;

	private Paint mPaint;
	private Paint mBitmapPaint;
	private Path mPath;
	private Bitmap mBitmap;
	private Canvas mCanvas;
	private boolean mIsEraseMode;

	public PaintingView(Context context) {
		super(context);
		init();
	}

	public PaintingView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public PaintingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if (mBitmap == null) {
			mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		}
		setBitmap(Bitmap.createBitmap(mBitmap, 0, 0, w, h));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float pointX = event.getX();
		float pointY = event.getY();
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mPath.reset();
				mPath.moveTo(pointX, pointY);
				break;
			case MotionEvent.ACTION_MOVE:
				mPath.lineTo(pointX, pointY);
				break;
			case MotionEvent.ACTION_UP:
				mPath.lineTo(pointX, pointY);
				mCanvas.drawPath(mPath, mPaint);
				mPath.reset();
				break;
			default:
				return false;
		}

		invalidate();
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
		canvas.drawPath(mPath, mPaint);
	}

	public void setColor(@ColorInt int color) {
		setEraseModeEnable(false);
		mPaint.setColor(color);
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public void setBitmap(@NonNull Bitmap bitmap) {
		mBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		mCanvas = new Canvas(mBitmap);
		invalidate();
	}

	public void setStrokeWidth(int width) {
		mPaint.setStrokeWidth(width);
	}

	public void setEraseModeEnable(boolean enabled) {
		// TODO implement this
	}

	private void init() {
		setFocusable(true);
		setFocusableInTouchMode(true);

		mBitmapPaint = new Paint(Paint.DITHER_FLAG);

		mPaint = new Paint();
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setColor(DEFAULT_COLOR);
		mPaint.setStrokeWidth(DEFAULT_STROKE_WIDTH);
		mPaint.setAntiAlias(true);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);

		mPath = new Path();
	}
}
