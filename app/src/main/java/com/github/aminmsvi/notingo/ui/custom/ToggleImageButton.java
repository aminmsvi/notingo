package com.github.aminmsvi.notingo.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;

import com.github.aminmsvi.notingo.R;

/**
 * Created by aminmsvi on 8/27/2017
 */
public class ToggleImageButton extends AppCompatImageButton implements Checkable, View.OnClickListener {

	private boolean mIsChecked;
	@Nullable
	private Drawable mDrawableOff;
	@Nullable
	private Drawable mDrawableOn;
	@Nullable
	private OnCheckedChangeListener mListener;

	public ToggleImageButton(Context context) {
		super(context);
		init(null);
	}

	public ToggleImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public ToggleImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(attrs);
	}

	public void setOnCheckChangedListener(OnCheckedChangeListener listener) {
		mListener = listener;
	}

	@Override
	public boolean isChecked() {
		return mIsChecked;
	}

	@Override
	public void setChecked(boolean checked) {
		setChecked(checked, true);
	}

	public void setChecked(boolean checked, boolean notifyListener) {
		mIsChecked = checked;
		refreshDrawableState();
		if (notifyListener && mListener != null) {
			mListener.onCheckedChanged(this, checked);
		}
	}

	@Override
	protected void drawableStateChanged() {
		if (mIsChecked) {
			setImageDrawable(mDrawableOn);
		} else {
			setImageDrawable(mDrawableOff);
		}
		super.drawableStateChanged();
	}

	@Override
	public void toggle() {
		setChecked(!mIsChecked);
	}

	@Override
	public void onClick(View v) {
		toggle();
	}

	private void init(@Nullable AttributeSet attrs) {
		setOnClickListener(this);

		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ToggleImageButton, 0, 0);
			int drawableOnResId = a.getResourceId(R.styleable.ToggleImageButton_onDrawable, -1);
			int drawableOffResId = a.getResourceId(R.styleable.ToggleImageButton_offDrawable, -1);
			boolean isChecked = a.getBoolean(R.styleable.ToggleImageButton_checked, false);
			a.recycle();

			AppCompatDrawableManager drawableManager = AppCompatDrawableManager.get();
			mDrawableOn = drawableOnResId != -1 ? drawableManager.getDrawable(getContext(), drawableOnResId) : null;
			mDrawableOff = drawableOffResId != -1 ? drawableManager.getDrawable(getContext(), drawableOffResId) : null;

			setChecked(isChecked);
		}
	}

	public interface OnCheckedChangeListener {

		void onCheckedChanged(ToggleImageButton buttonView, boolean isChecked);
	}
}
