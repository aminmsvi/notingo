package com.github.aminmsvi.notingo.ui.custom.styleableedittext;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.github.aminmsvi.notingo.R;

/**
 * Created by aminmsvi on 8/26/2017
 */
public class StyleableEditText extends AppCompatEditText implements ActionMode.Callback {

	private StyleableTextWatcher mStyleableTextWatcher;

	public StyleableEditText(Context context) {
		super(context);
		init();
	}

	public StyleableEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public StyleableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	@Override
	protected void onSelectionChanged(int selStart, int selEnd) {
		super.onSelectionChanged(selStart, selEnd);
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		MenuInflater menuInflater = mode.getMenuInflater();
		menuInflater.inflate(R.menu.menu_styleable_edit_text, menu);
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_bold:
				applyStyleToSelection(TextStyle.BOLD);
				return true;
			case R.id.action_italic:
				applyStyleToSelection(TextStyle.ITALIC);
				return true;
			case R.id.action_underline:
				applyStyleToSelection(TextStyle.UNDERLINE);
				return true;
		}
		return false;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {

	}

	public void addTextStyle(TextStyle textStyle) {
		mStyleableTextWatcher.addTextStyle(textStyle);
	}

	public void removeTextStyle(TextStyle textStyle) {
		mStyleableTextWatcher.removeTextStyle(textStyle);
	}

	public void clearAllTextStyles() {
		mStyleableTextWatcher.clearAllTextStyles();
	}

	private void init() {
		mStyleableTextWatcher = new StyleableTextWatcher(this);
		addTextChangedListener(mStyleableTextWatcher);
		setCustomSelectionActionModeCallback(this);
	}

	private void applyStyleToSelection(TextStyle textStyle) {
		Object span;
		if (textStyle == TextStyle.BOLD) {
			span = new StyleSpan(Typeface.BOLD);
		} else if (textStyle == TextStyle.UNDERLINE) {
			span = new UnderlineSpan();
		} else {
			span = new StyleSpan(Typeface.ITALIC);
		}
		getText().setSpan(span, getSelectionStart(), getSelectionEnd(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
	}
}
