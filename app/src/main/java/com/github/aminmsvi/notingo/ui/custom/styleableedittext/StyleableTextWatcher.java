package com.github.aminmsvi.notingo.ui.custom.styleableedittext;

import android.graphics.Typeface;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

import java.util.HashSet;

/**
 * Created by aminmsvi on 8/26/2017
 */
class StyleableTextWatcher implements TextWatcher {

	private final HashSet<TextStyle> mTextStyles;
	private final TextView mTextView;

	private int mStart;
	private int mCount;

	StyleableTextWatcher(TextView textView) {
		mTextView = textView;
		mTextStyles = new HashSet<>();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		mStart = start;
		mCount = count;
	}

	@Override
	public void afterTextChanged(Editable s) {
		mTextView.removeTextChangedListener(this);
		setSpans(s);
		mTextView.addTextChangedListener(this);
	}

	void addTextStyle(TextStyle textStyle) {
		mTextStyles.add(textStyle);
	}

	void removeTextStyle(TextStyle textStyle) {
		mTextStyles.remove(textStyle);
	}

	void clearAllTextStyles() {
		mTextStyles.clear();
	}

	private void setSpans(Editable editable) {
		if (mTextStyles.contains(TextStyle.UNDERLINE)) {
			editable.setSpan(new UnderlineSpan(), mStart, mStart + mCount, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		if (mTextStyles.contains(TextStyle.BOLD)) {
			editable.setSpan(new StyleSpan(Typeface.BOLD), mStart, mStart + mCount, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		if (mTextStyles.contains(TextStyle.ITALIC)) {
			editable.setSpan(new StyleSpan(Typeface.ITALIC), mStart, mStart + mCount, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
	}
}
