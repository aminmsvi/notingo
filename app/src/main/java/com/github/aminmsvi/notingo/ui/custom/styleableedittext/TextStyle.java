package com.github.aminmsvi.notingo.ui.custom.styleableedittext;

/**
 * Created by aminmsvi on 8/26/2017
 */
public enum TextStyle {

	BOLD, ITALIC, UNDERLINE
}
