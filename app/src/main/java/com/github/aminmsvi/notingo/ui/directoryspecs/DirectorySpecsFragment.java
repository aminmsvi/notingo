package com.github.aminmsvi.notingo.ui.directoryspecs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.ui.base.BaseFragment;

/**
 * Created by aminmsvi on 8/29/2017
 */
public class DirectorySpecsFragment extends BaseFragment implements DirectorySpecsMvpView, View.OnClickListener {

	private DirectorySpecsMvpPresenter<DirectorySpecsMvpView> mPresenter;
	private Listener mListener;

	private EditText mEdtName;

	public DirectorySpecsFragment() {
	}

	public static DirectorySpecsFragment newInstance() {
		return new DirectorySpecsFragment();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = new DirectorySpecsPresenter<>();
		mPresenter.attach(this);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_directory_specs, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mEdtName = view.findViewById(R.id.edt_create_directory_name);
		Button btnCreate = view.findViewById(R.id.btn_create_directory_create);

		btnCreate.setOnClickListener(this);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		try {
			mListener = (Listener) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(((Activity) context).getClass().getSimpleName()
					                             + " should implements " + Listener.class.getSimpleName());

		}
	}

	@Override
	public void onDetach() {
		mListener = null;
		super.onDetach();
	}

	@Override
	public void onDestroyView() {
		mPresenter.detach();
		super.onDestroyView();
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.btn_create_directory_create) {
			mPresenter.onBtnCreateClicked(mEdtName.getText());
		}
	}

	@Override
	public void showNameError() {
		mEdtName.setError(getString(R.string.directory_specs_error_empty_name));
	}

	@Override
	public void hideNameError() {
		mEdtName.setError(null);
	}

	@Override
	public void createDirectory(@NonNull String name) {
		mListener.onDirectorySpecsEntered(name);
	}

	public interface Listener {

		void onDirectorySpecsEntered(@NonNull String name);
	}
}
