package com.github.aminmsvi.notingo.ui.directoryspecs;

import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.ui.base.MvpPresenter;

/**
 * Created by aminmsvi on 8/29/2017
 */
interface DirectorySpecsMvpPresenter<V extends DirectorySpecsMvpView> extends MvpPresenter<V> {

	/**
	 * Notify presenter that create button is clicked.
	 *
	 * @param name Name of the directory.
	 */
	void onBtnCreateClicked(@Nullable CharSequence name);
}
