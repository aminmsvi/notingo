package com.github.aminmsvi.notingo.ui.directoryspecs;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.ui.base.MvpView;

/**
 * Created by aminmsvi on 8/29/2017
 */
interface DirectorySpecsMvpView extends MvpView {

	/**
	 * Show error for name.
	 */
	void showNameError();

	/**
	 * Hide error for name.
	 */
	void hideNameError();

	/**
	 * Tell fragment to create a directory with specified name should be created.
	 *
	 * @param name Name of the directory.
	 */
	void createDirectory(@NonNull String name);
}
