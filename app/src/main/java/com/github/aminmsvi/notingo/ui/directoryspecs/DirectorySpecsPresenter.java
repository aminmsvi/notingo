package com.github.aminmsvi.notingo.ui.directoryspecs;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.github.aminmsvi.notingo.ui.base.BasePresenter;

/**
 * Created by aminmsvi on 8/29/2017
 */
class DirectorySpecsPresenter<V extends DirectorySpecsMvpView> extends BasePresenter<V> implements DirectorySpecsMvpPresenter<V> {

	DirectorySpecsPresenter() {
		super();
	}

	@Override
	@SuppressWarnings("ConstantConditions")
	public void onBtnCreateClicked(@Nullable CharSequence name) {
		if (validate(name)) {
			getMvpView().createDirectory(name.toString());
			getMvpView().finish();
		}
	}

	private boolean validate(@Nullable CharSequence name) {
		if (TextUtils.isEmpty(name)) {
			getMvpView().showNameError();
			return false;
		}
		return true;
	}
}
