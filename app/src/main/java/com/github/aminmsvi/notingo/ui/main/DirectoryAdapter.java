package com.github.aminmsvi.notingo.ui.main;

import android.annotation.SuppressLint;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.github.aminmsvi.notingo.R;

/**
 * Created by aminmsvi on 8/28/2017
 */
class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ViewHolder> {

	private MainMvpPresenter<MainMvpView> mPresenter;

	DirectoryAdapter(MainMvpPresenter<MainMvpView> presenter) {
		mPresenter = presenter;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_directory, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		mPresenter.onBindDirectoryViewAtPosition(position, holder);
	}

	@Override
	public int getItemCount() {
		return mPresenter.getDirectoriesCount();
	}

	class ViewHolder extends RecyclerView.ViewHolder implements MainMvpView.DirectoryRowView,
			View.OnClickListener, PopupMenu.OnMenuItemClickListener {

		private TextView mTvName;
		private ImageButton mBtnOptions;

		ViewHolder(View itemView) {
			super(itemView);
			mTvName = itemView.findViewById(R.id.tv_item_directory_name);
			mTvName.setCompoundDrawablesWithIntrinsicBounds(
					AppCompatResources.getDrawable(itemView.getContext(), R.drawable.ic_folder_grey_24dp),   // Left
					null,   // Top
					null,   // Right
					null    // Bottom
			);
			mBtnOptions = itemView.findViewById(R.id.btn_item_directory_options);
			mBtnOptions.setOnClickListener(this);

			itemView.setOnClickListener(this);
		}

		@Override
		public void setName(String name) {
			mTvName.setText(name);
		}

		@SuppressLint("RtlHardcoded")
		@Override
		public void showOptionMenu() {
			PopupMenu popupMenu = new PopupMenu(itemView.getContext(), mBtnOptions, Gravity.RIGHT);
			popupMenu.inflate(R.menu.menu_item_directory);
			popupMenu.setOnMenuItemClickListener(this);
			popupMenu.show();
		}

		@Override
		public void onClick(View view) {
			if (view.getId() == R.id.btn_item_directory_options) {
				mPresenter.onBtnDirectoryOptionsClick(this);
			} else {
				mPresenter.onItemDirectoryClick(getLayoutPosition());
			}
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_delete:
					mPresenter.onBtnDirectoryDeleteClick(getLayoutPosition());
					return true;
			}
			return false;
		}
	}
}
