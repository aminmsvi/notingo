package com.github.aminmsvi.notingo.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryRepository;
import com.github.aminmsvi.notingo.executor.AppExecutors;
import com.github.aminmsvi.notingo.ui.base.BaseActivity;
import com.github.aminmsvi.notingo.ui.custom.FabMenu;
import com.github.aminmsvi.notingo.ui.directoryspecs.DirectorySpecsFragment;
import com.github.aminmsvi.notingo.ui.notelist.NoteListFragment;

public class MainActivity extends BaseActivity implements MainMvpView, DirectorySpecsFragment.Listener {

	private final static String TAG_NESTED_NOTES_FRAGMENT = "nested_notes_fragment";
	private final static String TAG_NOT_NESTED_NOTES_FRAGMENT = "not_nested_notes_fragment";

	private MainMvpPresenter<MainMvpView> mPresenter;
	private RecyclerView mRvDirectories;

	public static Intent getCallingIntent(@NonNull Context context) {
		return new Intent(context, MainActivity.class);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mPresenter = new MainPresenter<>(DirectoryRepository.getInstance(this, AppExecutors.getInstance()));
		mPresenter.attach(this);

		mRvDirectories = findViewById(R.id.rv_main_directories);
		DirectoryAdapter directoryAdapter = new DirectoryAdapter(mPresenter);
		mRvDirectories.setAdapter(directoryAdapter);
		mRvDirectories.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false));

		FabMenu fabMenu = findViewById(R.id.fm_main_menu);
		fabMenu.addMenuItem(R.drawable.ic_create_new_folder_black_24dp, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mPresenter.onBtnCreateNewDirectoryClick();
			}
		});
		fabMenu.addMenuItem(R.drawable.ic_create_new_note_black_24dp, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mPresenter.onBtnCreateNewTextNoteClick();
			}
		});
		fabMenu.addMenuItem(R.drawable.ic_brush_black_24dp, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				mPresenter.onBtnCreateNewPaintingNoteClick();
			}
		});

		getSupportFragmentManager().beginTransaction()
				.add(R.id.fl_main_notes_container, NoteListFragment.newInstance(true, DirectoryRepository.DIRECTORY_ROOT_ID), TAG_NESTED_NOTES_FRAGMENT)
				.commit();

		mPresenter.onCreate();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				mPresenter.onBtnBackClick();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		if (mPresenter != null) {
			mPresenter.detach();
		}
		super.onDestroy();
	}

	@Override
	public void showDirectorySpecsDialog() {
		DirectorySpecsFragment.newInstance()
				.show(getSupportFragmentManager(),
				      DirectorySpecsFragment.class.getSimpleName());
	}

	@Override
	public void openNote(@NonNull Note note) {
		NoteListFragment noteListFragment = (NoteListFragment) getSupportFragmentManager().findFragmentByTag(TAG_NESTED_NOTES_FRAGMENT);
		noteListFragment.openNote(note);
	}

	@Override
	public void notifyDirectoryItemRangeInserted(int startPosition, int itemCount) {
		mRvDirectories.getAdapter().notifyItemRangeInserted(startPosition, itemCount);
	}

	@Override
	public void notifyDirectoryItemInserted(int position) {
		mRvDirectories.getAdapter().notifyItemInserted(position);
	}

	@Override
	public void notifyDirectoryItemRemoved(int position) {
		mRvDirectories.getAdapter().notifyItemRemoved(position);
	}

	@Override
	public void onBackPressed() {
		// TODO modify this to notify presenter
		NoteListFragment noteListFragment = (NoteListFragment) getSupportFragmentManager().findFragmentByTag(TAG_NOT_NESTED_NOTES_FRAGMENT);
		if (noteListFragment != null) {
			mPresenter.onBtnBackClick();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onDirectorySpecsEntered(@NonNull String name) {
		mPresenter.onDirectorySpecsEntered(name);
	}

	@Override
	public void showNotesUnderDirectory(@NonNull String directoryId) {
		// hide import option menu item to prevent duplicates
		NoteListFragment nestedNoteListFragment = (NoteListFragment) getSupportFragmentManager().findFragmentByTag(TAG_NESTED_NOTES_FRAGMENT);
		nestedNoteListFragment.setHasOptionsMenu(false);
		invalidateOptionsMenu();

		getSupportFragmentManager().beginTransaction()
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
				.add(android.R.id.content, NoteListFragment.newInstance(false, directoryId), TAG_NOT_NESTED_NOTES_FRAGMENT)
				.commit();
	}

	@Override
	public void showNotesUnderDirectoryRoot() {
		NoteListFragment noteListFragment = (NoteListFragment) getSupportFragmentManager().findFragmentByTag(TAG_NOT_NESTED_NOTES_FRAGMENT);
		getSupportFragmentManager().beginTransaction()
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
				.remove(noteListFragment)
				.commitAllowingStateLoss();

		// hide import option menu item to prevent duplicates
		NoteListFragment nestedNoteListFragment = (NoteListFragment) getSupportFragmentManager().findFragmentByTag(TAG_NESTED_NOTES_FRAGMENT);
		nestedNoteListFragment.setHasOptionsMenu(true);
		invalidateOptionsMenu();
	}

	@Override
	public void setUpButtonEnable(boolean isEnabled) {
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(isEnabled);
			invalidateOptionsMenu();
		}
	}

	@Override
	public void showNoDirectoryMessage() {
		findViewById(R.id.tv_main_no_directory).setVisibility(View.VISIBLE);
	}

	@Override
	public void hideNoDirectoryMessage() {
		findViewById(R.id.tv_main_no_directory).setVisibility(View.GONE);
	}
}
