package com.github.aminmsvi.notingo.ui.main;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.ui.base.MvpPresenter;

/**
 * Created by aminmsvi on 8/25/2017
 */
interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {

	/**
	 * Perform things which needs to be done when view is created.
	 */
	void onCreate();

	/**
	 * Notify presenter that new directory button is clicked.
	 */
	void onBtnCreateNewDirectoryClick();

	/**
	 * Notify presenter that new text note button is clicked
	 */
	void onBtnCreateNewTextNoteClick();

	/**
	 * Notify presenter that new painting note button is clicked.
	 */
	void onBtnCreateNewPaintingNoteClick();

	/**
	 * Notify presenter that user has entered directory specifications.
	 *
	 * @param name Name of the directory
	 */
	void onDirectorySpecsEntered(@NonNull String name);

	/**
	 * Get count of directories.
	 *
	 * @return Count of directories.
	 */
	int getDirectoriesCount();

	/**
	 * Bind directory view to directory object at specified position.
	 *
	 * @param position         Position of directory to be bound to view.
	 * @param directoryRowView View of the directory to be bound to object.
	 */
	void onBindDirectoryViewAtPosition(int position, MainMvpView.DirectoryRowView directoryRowView);

	/**
	 * Notify presenter that directory options button is clicked.
	 *
	 * @param directoryRowView The view which its button is clicked.
	 */
	void onBtnDirectoryOptionsClick(MainMvpView.DirectoryRowView directoryRowView);

	/**
	 * Notify presenter that directory item view at position is clicked.
	 *
	 * @param position Position of clicked view item.
	 */
	void onItemDirectoryClick(int position);

	/**
	 * Notify presenter that directory delete button is clicked.
	 *
	 * @param position Position of clicked view item.
	 */
	void onBtnDirectoryDeleteClick(int position);

	/**
	 * Notify presenter that up button is clicked.
	 */
	void onBtnBackClick();
}
