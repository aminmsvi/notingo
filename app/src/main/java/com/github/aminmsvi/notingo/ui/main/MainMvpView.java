package com.github.aminmsvi.notingo.ui.main;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.base.MvpView;

/**
 * Created by aminmsvi on 8/25/2017
 */
interface MainMvpView extends MvpView {

	/**
	 * Show a dialog to user so user can specify directory specifications.
	 */
	void showDirectorySpecsDialog();

	/**
	 * Notify view of itemCount items inserted starting from startPosition so view can add these items.
	 *
	 * @param startPosition Position of first newly inserted item.
	 * @param itemCount     Count of the items which has inserted.
	 */
	void notifyDirectoryItemRangeInserted(int startPosition, int itemCount);

	/**
	 * Notify view of newly inserted item at position.
	 *
	 * @param position Position of newly inserted item.
	 */
	void notifyDirectoryItemInserted(int position);

	/**
	 * Notify view of removed item at position so view can remove it.
	 *
	 * @param position Position of removed item.
	 */
	void notifyDirectoryItemRemoved(int position);

	/**
	 * Show notes under directory which has the specified id.
	 *
	 * @param directoryId Id of the directory.
	 */
	void showNotesUnderDirectory(@NonNull String directoryId);

	/**
	 * Show notes which are under root directory.
	 */
	void showNotesUnderDirectoryRoot();

	/**
	 * Open a note in notepad.
	 *
	 * @param note Note to be opened in notepad.
	 */
	void openNote(@NonNull Note note);

	/**
	 * Set up (back button inside actionbar) button enable in view.
	 *
	 * @param isEnabled <c>true</c> if should be enabled otherwise <c>false</c>.
	 */
	void setUpButtonEnable(boolean isEnabled);

	/**
	 * Hide the no directory message
	 */
	void hideNoDirectoryMessage();

	/**
	 * Show the no directory message
	 */
	void showNoDirectoryMessage();

	/**
	 * Interface for communicating with directory view holder.
	 */
	interface DirectoryRowView {

		/**
		 * Set view's name text.
		 *
		 * @param name Name of the directory.
		 */
		void setName(String name);

		/**
		 * Show popup menu which contains directory item options.
		 */
		void showOptionMenu();
	}
}
