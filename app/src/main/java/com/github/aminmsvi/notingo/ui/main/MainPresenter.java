package com.github.aminmsvi.notingo.ui.main;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Directory;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryDataSource;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryRepository;
import com.github.aminmsvi.notingo.ui.base.BasePresenter;
import com.github.aminmsvi.notingo.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aminmsvi on 8/25/2017
 */
class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V>, DirectoryDataSource.ListCallback {

	private final DirectoryRepository mDirectoryRepository;
	private final List<Directory> mDirectories;

	private String mCurrentDirectoryId;

	MainPresenter(DirectoryRepository directoryRepository) {
		super();
		mDirectoryRepository = directoryRepository;
		mDirectories = new ArrayList<>();
	}

	@Override
	public void onCreate() {
		getMvpView().showLoading();
		mCurrentDirectoryId = DirectoryRepository.DIRECTORY_ROOT_ID;
		mDirectoryRepository.getDirectoriesWithParentId(DirectoryRepository.DIRECTORY_ROOT_ID, this);
	}

	@Override
	public int getDirectoriesCount() {
		return mDirectories.size();
	}

	@Override
	public void onBindDirectoryViewAtPosition(int position, MainMvpView.DirectoryRowView directoryRowView) {
		Directory directory = mDirectories.get(position);
		directoryRowView.setName(directory.getName());
	}

	@Override
	public void onBtnCreateNewDirectoryClick() {
		getMvpView().showDirectorySpecsDialog();
	}

	@Override
	public void onBtnCreateNewTextNoteClick() {
		// create text note
		Note note = new Note(mCurrentDirectoryId, "", "");
		getMvpView().openNote(note);
	}

	@Override
	public void onBtnCreateNewPaintingNoteClick() {
		// create painting note
		Note note = new Note(mCurrentDirectoryId, "");
		getMvpView().openNote(note);
	}

	@Override
	public void onDirectorySpecsEntered(@NonNull String name) {
		// Create a new directory
		Directory directory = new Directory(name, DirectoryRepository.DIRECTORY_ROOT_ID);
		mDirectoryRepository.saveDirectories(directory);
		mDirectories.add(directory);
		getMvpView().notifyDirectoryItemInserted(mDirectories.size() - 1);
		getMvpView().hideNoDirectoryMessage();
	}

	@Override
	public void onBtnDirectoryOptionsClick(MainMvpView.DirectoryRowView directoryRowView) {
		directoryRowView.showOptionMenu();
	}

	@Override
	public void onItemDirectoryClick(int position) {
		Directory directory = mDirectories.get(position);
		mCurrentDirectoryId = directory.getId();
		getMvpView().showNotesUnderDirectory(directory.getId());
		getMvpView().setUpButtonEnable(true);
	}

	@Override
	public void onBtnDirectoryDeleteClick(int position) {
		Directory directory = mDirectories.get(position);
		mDirectoryRepository.deleteDirectories(directory);
		mDirectories.remove(position);
		getMvpView().notifyDirectoryItemRemoved(position);
		if (mDirectories.isEmpty()) {
			getMvpView().showNoDirectoryMessage();
		}
	}

	@Override
	public void onBtnBackClick() {
		mCurrentDirectoryId = DirectoryRepository.DIRECTORY_ROOT_ID;
		getMvpView().showNotesUnderDirectoryRoot();
		getMvpView().setUpButtonEnable(false);
	}

	@Override
	public void onDirectoriesLoaded(@NonNull List<Directory> directories) {
		getMvpView().hideNoDirectoryMessage();
		int startPosition = mDirectories.size();
		mDirectories.addAll(directories);
		getMvpView().notifyDirectoryItemRangeInserted(startPosition, directories.size());
	}

	@Override
	public void onNoDataAvailable() {
		getMvpView().showNoDirectoryMessage();
		LogUtils.d("NotePresenter", "root directory is not available!");
	}
}
