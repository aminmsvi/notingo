package com.github.aminmsvi.notingo.ui.notelist;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.utils.NoteUtils;

/**
 * Created by aminmsvi on 8/27/2017
 */
class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {

	private final NoteListMvpPresenter<NoteListMvpView> mPresenter;

	NoteAdapter(NoteListMvpPresenter<NoteListMvpView> presenter) {
		mPresenter = presenter;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		mPresenter.onBindNoteRowViewAtPosition(position, holder);
	}

	@Override
	public int getItemCount() {
		return mPresenter.getNotesCount();
	}

	class ViewHolder extends RecyclerView.ViewHolder implements NoteListMvpView.NotesRowView,
			View.OnClickListener, PopupMenu.OnMenuItemClickListener {

		private TextView mTvTitle;
		private TextView mTvBody;
		private ImageView mImgPainting;
		private ImageButton mBtnOptions;

		ViewHolder(View itemView) {
			super(itemView);
			mTvTitle = itemView.findViewById(R.id.tv_item_note_title);
			mTvBody = itemView.findViewById(R.id.tv_item_note_body);
			mImgPainting = itemView.findViewById(R.id.img_item_note_painting);

			mBtnOptions = itemView.findViewById(R.id.btn_item_note_options);
			mBtnOptions.setOnClickListener(this);

			itemView.setOnClickListener(this);
		}

		public void setTitle(@NonNull String title) {
			mTvTitle.setText(NoteUtils.getHtmlAsText(title));
		}

		public void setBody(@NonNull String body) {
			mTvBody.setText(NoteUtils.getHtmlAsText(body));
		}

		@Override
		public void setPainting(@NonNull Bitmap painting) {
			mImgPainting.setImageBitmap(painting);
			mImgPainting.invalidate();
		}

		@Override
		public void hideTitle() {
			mTvTitle.setVisibility(View.GONE);
		}

		@Override
		public void hideBody() {
			mTvBody.setVisibility(View.GONE);
		}

		@Override
		public void hidePainting() {
			mImgPainting.setVisibility(View.GONE);
		}

		@SuppressLint("RtlHardcoded")
		@Override
		public void showOptionMenu() {
			PopupMenu popupMenu = new PopupMenu(itemView.getContext(), mBtnOptions, Gravity.RIGHT);
			popupMenu.inflate(R.menu.menu_item_note);
			popupMenu.setOnMenuItemClickListener(this);
			popupMenu.show();
		}

		@Override
		public void onClick(View view) {
			if (view.getId() == R.id.btn_item_note_options) {
				mPresenter.onBtnNoteOptionsClick(this);
			} else {
				mPresenter.onItemNoteClick(getLayoutPosition());
			}
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			switch (item.getItemId()) {
				case R.id.action_delete:
					mPresenter.onBtnNoteDeleteClick(getLayoutPosition());
					return true;
				case R.id.action_export:
					mPresenter.onBtnExportClick(getLayoutPosition());
			}
			return false;
		}
	}
}
