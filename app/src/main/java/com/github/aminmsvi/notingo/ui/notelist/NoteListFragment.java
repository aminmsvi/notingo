package com.github.aminmsvi.notingo.ui.notelist;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.Router;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.directory.DirectoryRepository;
import com.github.aminmsvi.notingo.data.source.note.NoteRepository;
import com.github.aminmsvi.notingo.executor.AppExecutors;
import com.github.aminmsvi.notingo.helper.PathHelper;
import com.github.aminmsvi.notingo.ui.base.BaseFragment;
import com.github.aminmsvi.notingo.ui.custom.FabMenu;

/**
 * Created by aminmsvi on 8/31/2017
 */
public class NoteListFragment extends BaseFragment implements NoteListMvpView {

	public static final String RESULT_NOTE_ID = "note_id";

	private final static int REQUEST_NOTE_PAD = 0x0;
	private final static int REQUEST_READ_PERMISSION = 0x2;
	private final static int REQUEST_WRITE_PERMISSION = 0x4;
	private final static int REQUEST_FILE_CHOOSER = 0x8;

	private static final String ARG_IS_NESTED = "is_nested";
	private static final String ARG_PARENT_DIRECTORY_ID = "parent_directory_id";

	private NoteListMvpPresenter<NoteListMvpView> mPresenter;

	private RecyclerView mRvNotes;

	public static NoteListFragment newInstance(boolean isNested, @NonNull String parentDirectoryId) {
		NoteListFragment noteListFragment = new NoteListFragment();
		noteListFragment.setHasOptionsMenu(true);
		Bundle bundle = new Bundle();
		bundle.putBoolean(ARG_IS_NESTED, isNested);
		bundle.putString(ARG_PARENT_DIRECTORY_ID, parentDirectoryId);
		noteListFragment.setArguments(bundle);
		return noteListFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_notes, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mPresenter = new NoteListPresenter<>(NoteRepository.getInstance(getBaseActivity().getApplicationContext(),
		                                                                AppExecutors.getInstance()),
		                                     new PathHelper(getContext()));
		mPresenter.attach(this);

		mRvNotes = view.findViewById(R.id.rv_notes);
		NoteAdapter noteAdapter = new NoteAdapter(mPresenter);
		mRvNotes.setAdapter(noteAdapter);
		mRvNotes.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

		FabMenu fabMenu = view.findViewById(R.id.fm_notes_menu);
		if (getArguments().getBoolean(ARG_IS_NESTED)) {
			fabMenu.setVisibility(View.GONE);
		} else {
			fabMenu.addMenuItem(R.drawable.ic_create_new_note_black_24dp, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					mPresenter.onBtnCreateNewTextNoteClick();
				}
			});
			fabMenu.addMenuItem(R.drawable.ic_brush_black_24dp, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					mPresenter.onBtnCreateNewPaintingNoteClick();
				}
			});
		}

		String parentDirectoryId = getArguments().getString(ARG_PARENT_DIRECTORY_ID, DirectoryRepository.DIRECTORY_ROOT_ID);
		mPresenter.onCreate(parentDirectoryId);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// prevent duplication!
		if (menu.findItem(R.id.action_import) != null) {
			menu.findItem(R.id.action_import).setVisible(false);
		}
		inflater.inflate(R.menu.menu_note_list, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_import:
				mPresenter.onBtnImportClick();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case REQUEST_NOTE_PAD:
				if (resultCode == Activity.RESULT_OK) {
					mPresenter.onNotePadResult(data.getStringExtra(RESULT_NOTE_ID));
				}
				break;
			case REQUEST_FILE_CHOOSER:
				if (resultCode == Activity.RESULT_OK) {
					mPresenter.onFileChooserResult(data.getData().getPath());
				}
				break;
		}
	}

	@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_WRITE_PERMISSION:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					mPresenter.onWritePermissionGranted();
				} else {
					if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle(R.string.note_list_write_permission_rationale_title);
						builder.setMessage(R.string.note_list_write_permission_rationale_message);
						builder.setPositiveButton(R.string.all_grant, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								requestPermissionsSafely(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
							}
						});
						builder.setNegativeButton(R.string.all_cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						builder.show();
					}
				}
				break;
			case REQUEST_READ_PERMISSION:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					mPresenter.onReadPermissionGranted();
				} else {
					if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
						builder.setTitle(R.string.main_read_permission_rationale_title);
						builder.setMessage(R.string.main_read_permission_rationale_message);
						builder.setPositiveButton(R.string.all_grant, new DialogInterface.OnClickListener() {

							@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								requestPermissionsSafely(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_PERMISSION);
							}
						});
						builder.setNegativeButton(R.string.all_cancel, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
							}
						});
						builder.show();
					}
				}
				break;
		}
	}

	@Override
	public void onDestroyView() {
		mPresenter.detach();
		super.onDestroyView();
	}

	@Override
	public void openNote(@NonNull Note note) {
		Router.navigateToNotePadActivityForResult(this, REQUEST_NOTE_PAD, note);
	}

	@Override
	public void notifyNoteItemRangeChanged(int startPosition, int itemCount) {
		mRvNotes.getAdapter().notifyItemRangeChanged(startPosition, itemCount);
	}

	@Override
	public void notifyNoteItemRemoved(int position) {
		mRvNotes.getAdapter().notifyItemRemoved(position);
	}

	@Override
	public void notifyNoteItemChanged(int position) {
		mRvNotes.getAdapter().notifyItemChanged(position);
	}

	@Override
	public void notifyNoteItemInserted(int position) {
		mRvNotes.getAdapter().notifyItemInserted(position);
	}

	@Override
	public boolean hasWritePermission() {
		return hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
	}

	@Override
	public void requestWritePermission() {
		requestPermissionsSafely(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
	}

	@Override
	public void showFileChooserDialog() {
		Intent intent = new Intent();
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		// Set your required file type
		intent.setType("text/xml|image/png");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, getString(R.string.main_file_chooser_title)), REQUEST_FILE_CHOOSER);
	}

	@Override
	public boolean hasReadPermission() {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN ||
				hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void requestReadPermission() {
		requestPermissionsSafely(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_PERMISSION);
	}
}
