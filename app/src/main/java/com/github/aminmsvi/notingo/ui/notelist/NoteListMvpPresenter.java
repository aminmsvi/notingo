package com.github.aminmsvi.notingo.ui.notelist;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.ui.base.MvpPresenter;

/**
 * Created by aminmsvi on 8/31/2017
 */
interface NoteListMvpPresenter<V extends NoteListMvpView> extends MvpPresenter<V> {

	/**
	 * Perform things which needs to be done when view is created.
	 */
	void onCreate(@NonNull String parentDirectoryId);

	/**
	 * Get count of notes.
	 *
	 * @return Count of notes.
	 */
	int getNotesCount();

	/**
	 * Bind note view to note object at specified position.
	 *
	 * @param position    Position of directory to be bound to view.
	 * @param noteRowView View of the note to be bound to object.
	 */
	void onBindNoteRowViewAtPosition(int position, NoteListMvpView.NotesRowView noteRowView);

	/**
	 * Notify presenter that create new text note button is clicked.
	 */
	void onBtnCreateNewTextNoteClick();

	/**
	 * Notify presenter that create new painting note button is clicked.
	 */
	void onBtnCreateNewPaintingNoteClick();

	/**
	 * Notify presenter that note delete button is clicked.
	 *
	 * @param position Position of clicked view item.
	 */
	void onBtnNoteDeleteClick(int position);

	/**
	 * Notify presenter that note item view at position is clicked.
	 *
	 * @param position Position of clicked view item.
	 */
	void onItemNoteClick(int position);

	/**
	 * Notify presenter that directory options button is clicked.
	 *
	 * @param noteRowView The view which its button is clicked.
	 */
	void onBtnNoteOptionsClick(NoteAdapter.ViewHolder noteRowView);

	/**
	 * Notify presenter that result of note pad is received.
	 *
	 * @param noteId Id of the new note created inside notepad.
	 */
	void onNotePadResult(@NonNull String noteId);

	/**
	 * Notify presenter that export button is clicked.
	 *
	 * @param position Position of clicked view item.
	 */
	void onBtnExportClick(int position);

	/**
	 * Notify presenter that user has granted write permission.
	 */
	void onWritePermissionGranted();

	/**
	 * Notify presenter that import button is clicked.
	 */
	void onBtnImportClick();

	/**
	 * Notify presenter that user has selected a note from storage.
	 *
	 * @param selectedNotePath Path to the selected note.
	 */
	void onFileChooserResult(@NonNull String selectedNotePath);

	/**
	 * Notify presenter that user has granted read external storage permission.
	 */
	void onReadPermissionGranted();
}
