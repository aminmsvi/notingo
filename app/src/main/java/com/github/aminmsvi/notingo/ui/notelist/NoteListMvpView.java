package com.github.aminmsvi.notingo.ui.notelist;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.base.MvpView;

/**
 * Created by aminmsvi on 8/31/2017
 */
interface NoteListMvpView extends MvpView {

	/**
	 * Open note in notepad.
	 *
	 * @param note Note which should be opened in notepad.
	 */
	void openNote(@NonNull Note note);

	/**
	 * Notify view of itemCount items changed starting from startPosition so view can apply changes to these items.
	 *
	 * @param startPosition Position of first newly inserted item.
	 * @param itemCount     Count of the items which has inserted.
	 */
	void notifyNoteItemRangeChanged(int startPosition, int itemCount);

	/**
	 * Notify view of removed item at position so view can remove it.
	 *
	 * @param position Position of removed item.
	 */
	void notifyNoteItemRemoved(int position);

	/**
	 * Notify view of changed item at position so view can change it.
	 *
	 * @param position Position of changed item.
	 */
	void notifyNoteItemChanged(int position);

	/**
	 * Notify view of inserted item at position so view can insert it.
	 *
	 * @param position Position of inserted item.
	 */
	void notifyNoteItemInserted(int position);

	/**
	 * Check if application has write permission.
	 *
	 * @return true if yes, otherwise return false.
	 */
	boolean hasWritePermission();

	/**
	 * Request for write permission.
	 */
	void requestWritePermission();

	/**
	 * Show a file chooser, so user can select a note from local storage.
	 */
	void showFileChooserDialog();

	/**
	 * Check if application has read external storage permission.
	 *
	 * @return true if yes, otherwise false.
	 */
	boolean hasReadPermission();

	/**
	 * Request read external storage from user.
	 */
	void requestReadPermission();

	/**
	 * Interface for communicating with note view holder.
	 */
	interface NotesRowView {

		/**
		 * Set view's title text.
		 *
		 * @param title Title of the view.
		 */
		void setTitle(@NonNull String title);

		/**
		 * Set view's body text.
		 *
		 * @param body Body of the note.
		 */
		void setBody(@NonNull String body);

		/**
		 * Set view's painting image.
		 *
		 * @param painting Image of the note.
		 */
		void setPainting(@NonNull Bitmap painting);

		/**
		 * Hide title text.
		 */
		void hideTitle();

		/**
		 * Hide body text.
		 */
		void hideBody();

		/**
		 * Hide painting image.
		 */
		void hidePainting();

		/**
		 * Show popup menu which contains directory item options.
		 */
		void showOptionMenu();
	}
}
