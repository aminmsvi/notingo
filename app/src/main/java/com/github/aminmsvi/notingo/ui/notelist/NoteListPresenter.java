package com.github.aminmsvi.notingo.ui.notelist;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.note.NoteDataSource;
import com.github.aminmsvi.notingo.data.source.note.NoteRepository;
import com.github.aminmsvi.notingo.helper.PathHelper;
import com.github.aminmsvi.notingo.ui.base.BasePresenter;
import com.github.aminmsvi.notingo.utils.LogUtils;
import com.github.aminmsvi.notingo.utils.NoteUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by aminmsvi on 8/31/2017
 */
class NoteListPresenter<V extends NoteListMvpView> extends BasePresenter<V> implements NoteListMvpPresenter<V>, NoteDataSource.ListCallback, NoteDataSource.SingleCallback {

	private final static String TAG = "NOTE_LIST_PRESENTER";

	private final NoteRepository mNoteRepository;
	private final PathHelper mPathHelper;
	private final List<Note> mNotes;

	private String mCurrentDirectoryId;
	private String mSelectedNotePathToImport;
	private int mNoteToExportPosition;

	NoteListPresenter(NoteRepository noteRepository, PathHelper pathHelper) {
		super();
		mNoteRepository = noteRepository;
		mPathHelper = pathHelper;
		mNotes = new ArrayList<>();
	}

	@Override
	public void onCreate(@NonNull String parentDirectoryId) {
		mNotes.clear();
		mCurrentDirectoryId = parentDirectoryId;
		mNoteRepository.getNotesWithDirectoryId(parentDirectoryId, this);
	}

	@Override
	public int getNotesCount() {
		return mNotes.size();
	}

	@Override
	public void onBindNoteRowViewAtPosition(int position, NoteListMvpView.NotesRowView noteRowView) {
		Note note = mNotes.get(position);
		if (note.getType() == Note.NOTE_TYPE_TEXT) {
			noteRowView.setTitle(note.getTitle());
			noteRowView.setBody(note.getBody());
			noteRowView.hidePainting();
		} else {
			Bitmap painting = NoteUtils.loadPainting(note.getPaintingPath());
			if (painting != null) {
				noteRowView.setPainting(painting);
			}
			noteRowView.hideTitle();
			noteRowView.hideBody();
		}
	}

	@Override
	public void onBtnCreateNewTextNoteClick() {
		// create text note
		Note note = new Note(mCurrentDirectoryId, "", "");
		getMvpView().openNote(note);
	}

	@Override
	public void onBtnCreateNewPaintingNoteClick() {
		// create painting note
		Note note = new Note(mCurrentDirectoryId, "");
		getMvpView().openNote(note);
	}

	@Override
	public void onBtnNoteDeleteClick(int position) {
		Note note = mNotes.get(position);
		mNoteRepository.deleteNotes(note);
		mNotes.remove(position);
		getMvpView().notifyNoteItemRemoved(position);
	}

	@Override
	public void onNotePadResult(@NonNull final String noteId) {
		mNoteRepository.getNoteWithId(noteId, this);
	}

	@Override
	public void onItemNoteClick(int position) {
		Note note = mNotes.get(position);
		getMvpView().openNote(note);
	}

	@Override
	public void onBtnNoteOptionsClick(NoteAdapter.ViewHolder noteRowView) {
		noteRowView.showOptionMenu();
	}

	@Override
	public void onWritePermissionGranted() {
		exportNote();
	}

	@Override
	public void onBtnExportClick(int position) {
		mNoteToExportPosition = position;
		if (getMvpView().hasWritePermission()) {
			exportNote();
		} else {
			getMvpView().requestWritePermission();
		}
	}

	@Override
	public void onBtnImportClick() {
		getMvpView().showFileChooserDialog();
	}

	@Override
	public void onFileChooserResult(@NonNull String selectedNotePath) {
		mSelectedNotePathToImport = selectedNotePath;
		if (getMvpView().hasReadPermission()) {
			importNote();
		} else {
			getMvpView().requestReadPermission();
		}
	}

	@Override
	public void onReadPermissionGranted() {
		importNote();
	}

	@Override
	public void onNotesLoaded(@NonNull List<Note> notes) {
		mNotes.addAll(notes);
		getMvpView().notifyNoteItemRangeChanged(0, notes.size());
	}

	@Override
	public void onNoteLoaded(@NonNull Note note) {
		for (int i = 0; i < mNotes.size(); i++) {
			Note item = mNotes.get(i);
			if (item.getId().equals(note.getId())) {
				mNotes.set(i, note);
				getMvpView().notifyNoteItemChanged(i);
				return;
			}
		}
		mNotes.add(note);
		getMvpView().notifyNoteItemInserted(mNotes.size() - 1);
	}

	@Override
	public void onNoDataAvailable() {
		LogUtils.d(TAG, "no data available!");
	}

	private void exportNote() {
		if (mPathHelper.isExternalStorageWritable()) {
			Note note = mNotes.get(mNoteToExportPosition);
			String path = null;
			switch (note.getType()) {
				case Note.NOTE_TYPE_TEXT:
					String xmlNote = NoteUtils.writeTextNoteToXml(note);
					if (xmlNote != null) {
						path = NoteUtils.saveTextNote(mPathHelper.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Notingo" + File.separator,
						                              note.getId(), xmlNote);
					}
					break;
				case Note.NOTE_TYPE_PAINTING:
					Bitmap painting = NoteUtils.loadPainting(note.getPaintingPath());
					if (painting != null) {
						path = NoteUtils.savePainting(mPathHelper.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Notingo" + File.separator,
						                              note.getId(), painting);
					}
					break;
			}

			if (path != null) {
				getMvpView().showMessage(R.string.note_list_export_successful);
			} else {
				getMvpView().showError(R.string.note_list_export_fail);
			}
		} else {
			getMvpView().showError(R.string.all_error_no_writable_external_sd);
		}
	}

	private void importNote() {
		Note note = new Note();
		note.setDirectoryId(mCurrentDirectoryId);
		if (mSelectedNotePathToImport.endsWith("xml")) {
			String xml = NoteUtils.readTextNoteXml(mSelectedNotePathToImport);
			if (xml != null) {
				Note parsedNote = NoteUtils.parseTextNoteFromXml(xml);
				if (parsedNote != null) {
					note.setTitle(parsedNote.getTitle());
					note.setBody(parsedNote.getBody());
					note.setType(Note.NOTE_TYPE_TEXT);
					mNoteRepository.saveNotes(note);
					mNotes.add(note);
					getMvpView().notifyNoteItemInserted(mNotes.size());
				}
			} else {
				getMvpView().showError(R.string.note_list_failed_to_import);
			}
		} else if (mSelectedNotePathToImport.endsWith("png")) {
			Bitmap painting = NoteUtils.loadPainting(mSelectedNotePathToImport);
			if (painting != null) {
				String paintingPath = NoteUtils.savePainting(mPathHelper.getFileDir().getAbsolutePath(), note.getId(), painting);
				if (paintingPath != null) {
					note.setPaintingPath(paintingPath);
					note.setType(Note.NOTE_TYPE_PAINTING);
					mNoteRepository.saveNotes(note);
					mNotes.add(note);
					getMvpView().notifyNoteItemInserted(mNotes.size());
				}
			} else {
				getMvpView().showError(R.string.note_list_failed_to_import);
			}
		} else {
			getMvpView().showError(R.string.main_error_not_supported_file);
		}
	}
}
