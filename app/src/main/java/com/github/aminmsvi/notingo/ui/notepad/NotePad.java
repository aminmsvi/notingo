package com.github.aminmsvi.notingo.ui.notepad;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by aminmsvi on 8/31/2017
 *
 * An interface which all notepad variation can implement.
 */
interface NotePad {

	/**
	 * Get title text of notepad.
	 *
	 * @return Title text of the note.
	 */
	@NonNull
	String getTitle();

	/**
	 * Set title text of the note.
	 *
	 * @param title Text to set as title.
	 */
	void setTitle(@NonNull String title);

	/**
	 * Get body text of note.
	 *
	 * @return Body text of the note.
	 */
	@NonNull
	String getBody();

	/**
	 * Set body text of the note.
	 *
	 * @param body Body to set as title.
	 */
	void setBody(@NonNull String body);

	/**
	 * Get painting bitmap of note.
	 *
	 * @return Painting bitmap of the note.
	 */
	@Nullable
	Bitmap getPainting();

	/**
	 * Set painting bitmap of note.
	 *
	 * @param painting Painting bitmap of note.
	 */
	void setPainting(@Nullable Bitmap painting);
}
