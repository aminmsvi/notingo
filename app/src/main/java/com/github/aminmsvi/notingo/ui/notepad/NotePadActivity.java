package com.github.aminmsvi.notingo.ui.notepad;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.note.NoteRepository;
import com.github.aminmsvi.notingo.executor.AppExecutors;
import com.github.aminmsvi.notingo.helper.PathHelper;
import com.github.aminmsvi.notingo.ui.base.BaseActivity;
import com.github.aminmsvi.notingo.ui.notelist.NoteListFragment;

/**
 * Created by aminmsvi on 8/31/2017
 */
public class NotePadActivity extends BaseActivity implements NotePadMvpView {

	private static final String EXTRA_NOTE = "note";
	private static final String TAG_NOTEPAD = "notepad";

	private NotePadMvpPresenter<NotePadMvpView> mPresenter;
	private NotePad mNotePad;

	public static Intent getCallingIntent(@NonNull Context context, @NonNull Note note) {
		Intent intent = new Intent(context, NotePadActivity.class);
		intent.putExtra(EXTRA_NOTE, note);
		return intent;
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);
		mPresenter = new NotePadPresenter<>(NoteRepository.getInstance(this, AppExecutors.getInstance()),
		                                    new PathHelper(this));
		mPresenter.attach(this);

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

		Note note = getIntent().getParcelableExtra(EXTRA_NOTE);
		switch (note.getType()) {
			case Note.NOTE_TYPE_TEXT:
				TextNotepadFragment textNotepad = TextNotepadFragment.newInstance(note);
				getSupportFragmentManager().beginTransaction()
						.add(R.id.fl_note_container, textNotepad, TAG_NOTEPAD)
						.commit();
				mNotePad = textNotepad;
				break;
			case Note.NOTE_TYPE_PAINTING:
				PaintingNotepadFragment paintingNotepad = PaintingNotepadFragment.newInstance(note);
				getSupportFragmentManager().beginTransaction()
						.add(R.id.fl_note_container, paintingNotepad, TAG_NOTEPAD)
						.commit();
				mNotePad = paintingNotepad;
				break;
		}

		mPresenter.onCreate(note);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_notepad, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_note_save:
				mPresenter.onBtnSaveClick(mNotePad.getTitle(), mNotePad.getBody(), mNotePad.getPainting());
				break;
			case android.R.id.home:
				onBackPressed();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		mPresenter.onBtnBackClick();
	}

	@Override
	protected void onDestroy() {
		mPresenter = null;
		super.onDestroy();
	}

	@Override
	public void setResult(boolean isSuccessful, @NonNull String noteId) {
		Intent intent = new Intent();
		intent.putExtra(NoteListFragment.RESULT_NOTE_ID, noteId);
		setResult(isSuccessful ? RESULT_OK : RESULT_CANCELED, intent);
	}

	@Override
	public void performBackPressed() {
		super.onBackPressed();
	}
}
