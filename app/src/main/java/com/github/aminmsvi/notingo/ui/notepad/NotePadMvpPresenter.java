package com.github.aminmsvi.notingo.ui.notepad;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.base.MvpPresenter;

/**
 * Created by aminmsvi on 8/31/2017
 */
interface NotePadMvpPresenter<V extends NotePadMvpView> extends MvpPresenter<V> {

	/**
	 * Perform things which needs to be done when view is created.
	 *
	 * @param note Note object which passed to view.
	 */
	void onCreate(@NonNull Note note);

	/**
	 * Notify presenter that save button is clicked.
	 */
	void onBtnSaveClick(@NonNull String title, @NonNull String body, @Nullable Bitmap painting);

	/**
	 * Notify presenter that back button is clicked.
	 */
	void onBtnBackClick();
}
