package com.github.aminmsvi.notingo.ui.notepad;

import android.support.annotation.NonNull;

import com.github.aminmsvi.notingo.ui.base.MvpView;

/**
 * Created by aminmsvi on 8/31/2017
 */
interface NotePadMvpView extends MvpView {

	/**
	 * Set result of this activity.
	 *
	 * @param successful Indicate if creating the note was successful.
	 * @param noteId     Id of the note which created.
	 */
	void setResult(boolean successful, @NonNull String noteId);

	/**
	 * Close view using back button press!
	 */
	void performBackPressed();
}
