package com.github.aminmsvi.notingo.ui.notepad;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.data.source.note.NoteRepository;
import com.github.aminmsvi.notingo.helper.PathHelper;
import com.github.aminmsvi.notingo.ui.base.BasePresenter;
import com.github.aminmsvi.notingo.utils.NoteUtils;

/**
 * Created by aminmsvi on 8/31/2017
 */
class NotePadPresenter<V extends NotePadMvpView> extends BasePresenter<V> implements NotePadMvpPresenter<V> {

	private final NoteRepository mNoteRepository;
	@NonNull
	private final PathHelper mPathHelper;

	private Note mCurrentNote;
	private boolean mIsSaved;

	NotePadPresenter(@NonNull NoteRepository noteRepository, @NonNull PathHelper pathHelper) {
		super();
		mNoteRepository = noteRepository;
		mPathHelper = pathHelper;
	}

	@Override
	public void onBtnBackClick() {
		getMvpView().setResult(mIsSaved, mCurrentNote.getId());
		getMvpView().performBackPressed();
	}

	@Override
	public void onCreate(@NonNull Note note) {
		mCurrentNote = note;
	}

	@Override
	public void onBtnSaveClick(@NonNull String title, @NonNull String body, @Nullable Bitmap painting) {
		if (isNoteValid(title, body)) {
			mCurrentNote.setTitle(title);
			mCurrentNote.setBody(body);
			if (painting != null) {
				String paintingPath = NoteUtils.savePainting(mPathHelper.getFileDir().getAbsolutePath(),
				                                             mCurrentNote.getId(), painting);
				if (paintingPath != null) {
					mCurrentNote.setPaintingPath(paintingPath);
				}
			}
			mNoteRepository.saveNotes(mCurrentNote);
			getMvpView().showMessage(R.string.notepad_note_saved);
			mIsSaved = true;
		}
	}

	private boolean isNoteValid(String title, String body) {
		if (mCurrentNote.getType() == Note.NOTE_TYPE_TEXT) {
			if (title.length() == 0) {
				return false;
			} else if (body.length() == 0) {
				return false;
			}
		} else {
			// TODO validate painting note
		}
		return true;
	}
}
