package com.github.aminmsvi.notingo.ui.notepad;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.custom.PaintingView;
import com.github.aminmsvi.notingo.ui.custom.ToggleImageButton;
import com.github.aminmsvi.notingo.utils.NoteUtils;

/**
 * Created by aminmsvi on 9/1/2017
 */
public class PaintingNotepadFragment extends Fragment implements NotePad, ToggleImageButton.OnCheckedChangeListener {

	private final static int COLOR_BLACK = 0xFF000000;
	private final static int COLOR_RED = 0xFFD50000;
	private final static int COLOR_GREEN = 0xFF00C853;
	private final static int COLOR_BLUE = 0xFF2962FF;

	private final static String ARG_NOTE = "note";

	private PaintingView mPainting;
	private ToggleImageButton mTbtnColorBlack;
	private ToggleImageButton mTbtnColorRed;
	private ToggleImageButton mTbtnColorBlue;
	private ToggleImageButton mTbtnColorGreen;
	private ToggleImageButton mTbtnEraser;

	public PaintingNotepadFragment() {
	}

	public static PaintingNotepadFragment newInstance(@Nullable Note note) {
		PaintingNotepadFragment fragment = new PaintingNotepadFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable(ARG_NOTE, note);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO create an spinner for changing stroke width
		return inflater.inflate(R.layout.fragment_painting_notepad, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mPainting = view.findViewById(R.id.pv_painting_notepad);
		// TODO create a custom radio button for this!!!
		mTbtnColorBlack = view.findViewById(R.id.tbtn_painting_notepad_black);
		mTbtnColorRed = view.findViewById(R.id.tbtn_painting_notepad_red);
		mTbtnColorBlue = view.findViewById(R.id.tbtn_painting_notepad_blue);
		mTbtnColorGreen = view.findViewById(R.id.tbtn_painting_notepad_green);
		mTbtnEraser = view.findViewById(R.id.tbtn_painting_notepad_eraser);

		mTbtnColorBlack.setOnCheckChangedListener(this);
		mTbtnColorGreen.setOnCheckChangedListener(this);
		mTbtnColorRed.setOnCheckChangedListener(this);
		mTbtnColorBlue.setOnCheckChangedListener(this);
		mTbtnEraser.setOnCheckChangedListener(this);

		mTbtnColorBlack.setChecked(true);

		Note note = getArguments().getParcelable(ARG_NOTE);
		if (note != null) {
			Bitmap painting = NoteUtils.loadPainting(note.getPaintingPath());
			setPainting(painting);
		}
	}

	@NonNull
	@Override
	public String getTitle() {
		return "";
	}

	@Override
	public void setTitle(@NonNull String title) {
		// Painting note does not have title
	}

	@NonNull
	@Override
	public String getBody() {
		return "";
	}

	@Override
	public void setBody(@NonNull String body) {
		// Painting note does not have text body
	}

	@Nullable
	@Override
	public Bitmap getPainting() {
		return mPainting.getBitmap();
	}

	@Override
	public void setPainting(@Nullable Bitmap painting) {
		if (painting != null) {
			mPainting.setBitmap(painting);
		}
	}

	@Override
	public void onCheckedChanged(ToggleImageButton buttonView, boolean isChecked) {
		mTbtnColorBlack.setChecked(false, false);
		mTbtnColorRed.setChecked(false, false);
		mTbtnColorBlue.setChecked(false, false);
		mTbtnColorGreen.setChecked(false, false);
		mTbtnEraser.setChecked(false, false);

		switch (buttonView.getId()) {
			case R.id.tbtn_painting_notepad_black:
				mPainting.setColor(COLOR_BLACK);
				mTbtnColorBlack.setChecked(true, false);
				break;
			case R.id.tbtn_painting_notepad_red:
				mPainting.setColor(COLOR_RED);
				mTbtnColorRed.setChecked(true, false);
				break;
			case R.id.tbtn_painting_notepad_blue:
				mPainting.setColor(COLOR_BLUE);
				mTbtnColorBlue.setChecked(true, false);
				break;
			case R.id.tbtn_painting_notepad_green:
				mPainting.setColor(COLOR_GREEN);
				mTbtnColorGreen.setChecked(true, false);
				break;
			case R.id.tbtn_painting_notepad_eraser:
				mPainting.setEraseModeEnable(true);
				mTbtnEraser.setChecked(true, false);
				break;
		}
	}
}
