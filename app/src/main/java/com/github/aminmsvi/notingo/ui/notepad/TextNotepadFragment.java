package com.github.aminmsvi.notingo.ui.notepad;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.aminmsvi.notingo.R;
import com.github.aminmsvi.notingo.data.entity.Note;
import com.github.aminmsvi.notingo.ui.custom.ToggleImageButton;
import com.github.aminmsvi.notingo.ui.custom.styleableedittext.StyleableEditText;
import com.github.aminmsvi.notingo.ui.custom.styleableedittext.TextStyle;
import com.github.aminmsvi.notingo.utils.NoteUtils;

public class TextNotepadFragment extends Fragment implements NotePad, ToggleImageButton.OnCheckedChangeListener, View.OnFocusChangeListener {

	private final static String ARG_NOTE = "note";

	private StyleableEditText mSedtTitle;
	private StyleableEditText mSedtBody;
	private ToggleImageButton mTbtnBold;
	private ToggleImageButton mTbtnItalic;
	private ToggleImageButton mTbtnUnderline;

	public TextNotepadFragment() {
	}

	public static TextNotepadFragment newInstance(@Nullable Note note) {
		TextNotepadFragment fragment = new TextNotepadFragment();
		Bundle bundle = new Bundle();
		bundle.putParcelable(ARG_NOTE, note);
		fragment.setArguments(bundle);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_text_notepad, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSedtBody = view.findViewById(R.id.sedt_text_note_body);
		mSedtTitle = view.findViewById(R.id.sedt_text_note_title);

		mTbtnBold = view.findViewById(R.id.tbtn_text_note_bold);
		mTbtnBold.setOnCheckChangedListener(this);
		mTbtnItalic = view.findViewById(R.id.tbtn_text_note_italic);
		mTbtnItalic.setOnCheckChangedListener(this);
		mTbtnUnderline = view.findViewById(R.id.tbtn_text_note_underline);
		mTbtnUnderline.setOnCheckChangedListener(this);

		mSedtTitle.setOnFocusChangeListener(this);
		mSedtBody.setOnFocusChangeListener(this);

		Note note = getArguments().getParcelable(ARG_NOTE);
		if (note != null) {
			setTitle(note.getTitle());
			setBody(note.getBody());
		}
	}

	@Override
	public void onCheckedChanged(ToggleImageButton buttonView, boolean isChecked) {
		TextStyle textStyle;
		int i = buttonView.getId();
		if (i == R.id.tbtn_text_note_bold) {
			textStyle = TextStyle.BOLD;
		} else if (i == R.id.tbtn_text_note_italic) {
			textStyle = TextStyle.ITALIC;
		} else {
			textStyle = TextStyle.UNDERLINE;
		}

		StyleableEditText focusedSedt;
		if (mSedtTitle.isFocused()) {
			focusedSedt = mSedtTitle;
		} else {
			focusedSedt = mSedtBody;
		}

		if (isChecked) {
			focusedSedt.addTextStyle(textStyle);
		} else {
			focusedSedt.removeTextStyle(textStyle);
		}
	}

	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		if (hasFocus) {
			StyleableEditText sedt = (StyleableEditText) view;
			sedt.clearAllTextStyles();
			if (mTbtnBold.isChecked()) {
				sedt.addTextStyle(TextStyle.BOLD);
			}
			if (mTbtnItalic.isChecked()) {
				sedt.addTextStyle(TextStyle.ITALIC);
			}
			if (mTbtnUnderline.isChecked()) {
				sedt.addTextStyle(TextStyle.UNDERLINE);
			}
		}
	}

	@Nullable
	@Override
	public Bitmap getPainting() {
		return null;
	}

	@Override
	public void setPainting(@Nullable Bitmap painting) {
		// currently this notepad does not support painting :D
	}

	@NonNull
	@Override
	public String getTitle() {
		return NoteUtils.getTextAsHtml(mSedtTitle.getText());
	}

	@Override
	public void setTitle(@NonNull String title) {
		mSedtTitle.setText(NoteUtils.getHtmlAsText(title));
	}

	@NonNull
	@Override
	public String getBody() {
		return NoteUtils.getTextAsHtml(mSedtBody.getText());
	}

	@Override
	public void setBody(@NonNull String body) {
		mSedtBody.setText(NoteUtils.getHtmlAsText(body));
	}
}
