package com.github.aminmsvi.notingo.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.github.aminmsvi.notingo.BuildConfig;

/**
 * Created by aminmsvi on 8/29/2017
 */
@SuppressWarnings("WeakerAccess")
public class LogUtils {

	private LogUtils() {
	}

	public static void d(@NonNull String tag, @NonNull String message) {
		if (BuildConfig.DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void e(@NonNull String tag, @NonNull Exception e) {
		if (BuildConfig.DEBUG) {
			Log.e(tag, "", e);
		}
	}

	public static void e(@NonNull String tag, @NonNull String message) {
		if (BuildConfig.DEBUG) {
			Log.e(tag, message);
		}
	}
}
