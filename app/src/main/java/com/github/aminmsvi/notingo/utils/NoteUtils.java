package com.github.aminmsvi.notingo.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Xml;

import com.github.aminmsvi.notingo.data.entity.Note;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by aminmsvi on 8/31/2017
 */
public class NoteUtils {

	private final static String PAINTING_EXTENSION = ".png";
	private final static String TEXT_EXTENTION = ".xml";
	private final static String PAINTING_DIRECTORY_NAME = "painting_notes";
	private final static String TEXT_DIRECTORY_NAME = "text_notes";

	private final static String XML_TAG_NOTE = "note";
	private final static String XML_TAG_TITLE = "title";
	private final static String XML_TAG_BODY = "body";

	private NoteUtils() {
	}

	/**
	 * Convert specified spannable text into HTML.
	 *
	 * @param text Text to be converted.
	 * @return HTML of the specified spannable.
	 */
	@SuppressWarnings("deprecation")
	public static String getTextAsHtml(Spannable text) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return Html.toHtml(text, Html.FROM_HTML_MODE_COMPACT);
		} else {
			return Html.toHtml(text);
		}
	}

	/**
	 * Convert specified HTML into a spanned text. This method also
	 * removes unwanted whitespaces from start and end of the HTML.
	 *
	 * @param html HTML to be converted.
	 * @return Spanned text of the specified HTML.
	 */
	@SuppressWarnings("deprecation")
	public static Spanned getHtmlAsText(String html) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return (Spanned) trim(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
		} else {
			return (Spanned) trim(Html.fromHtml(html));
		}
	}

	/**
	 * Save painting to private file directory of application.
	 *
	 * @param path   Path of the directory to store bitmap.
	 * @param name   Name of the painting bitmap.
	 * @param bitmap Painting bitmap.
	 * @return If successful, absolute path to the bitmap otherwise null.
	 */
	@Nullable
	public static String savePainting(@NonNull String path, @NonNull String name, @NonNull Bitmap bitmap) {
		String directory = path + File.separator + PAINTING_DIRECTORY_NAME;
		boolean result = false;
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				LogUtils.e("SAVE_PAINTING", "Failed to make directories");
				return null;
			}
		}
		FileOutputStream fileOutputStream = null;
		File file = new File(dir, name + PAINTING_EXTENSION);
		try {
			fileOutputStream = new FileOutputStream(file);
			result = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
		} catch (Exception e) {
			LogUtils.e("SAVE_PAINTING", e);
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					LogUtils.e("SAVE_PAINTING", e);
				}
			}
		}
		if (result) {
			return file.getAbsolutePath();
		} else {
			return null;
		}
	}

	/**
	 * Load painting bitmap from provided path.
	 *
	 * @param path Path of the bitmap.
	 * @return Bitmap stored in the specified path, if there was any!
	 */
	@Nullable
	public static Bitmap loadPainting(@NonNull String path) {
		Bitmap bitmap = null;
		try {
			File file = new File(path);
			bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			LogUtils.e("SAVE_PAINTING", e);
		}
		return bitmap;
	}

	@Nullable
	public static String saveTextNote(@NonNull String path, @NonNull String name, @NonNull String xmlNote) {
		String directory = path + File.separator + TEXT_DIRECTORY_NAME;
		boolean result = false;
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				LogUtils.e("SAVE_TEXT", "Failed to make directories");
				return null;
			}
		}
		FileOutputStream fileOutputStream = null;
		File file = new File(dir, name + TEXT_EXTENTION);
		try {
			fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(xmlNote.getBytes());
			result = true;
		} catch (Exception e) {
			LogUtils.e("SAVE_TEXT", e);
		} finally {
			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (IOException e) {
					LogUtils.e("SAVE_PAINTING", e);
				}
			}
		}
		if (result) {
			return file.getAbsolutePath();
		} else {
			return null;
		}
	}

	@Nullable
	public static String writeTextNoteToXml(@NonNull Note note) {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", XML_TAG_NOTE);
			serializer.startTag("", XML_TAG_TITLE);
			serializer.text(note.getTitle());
			serializer.endTag("", XML_TAG_TITLE);
			serializer.startTag("", XML_TAG_BODY);
			serializer.text(note.getBody());
			serializer.endTag("", XML_TAG_BODY);
			serializer.endTag("", XML_TAG_NOTE);
			serializer.endDocument();
			return writer.toString();
		} catch (Exception e) {
			LogUtils.e("WRITE_NOTE_XML", e);
			return null;
		}
	}

	@Nullable
	public static String readTextNoteXml(@NonNull String path) {
		File file = new File(path);
		StringBuilder text = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				text.append(line);
				text.append('\n');
			}
			reader.close();
		} catch (IOException e) {
			LogUtils.e("READING_XML", e);
			return null;
		}
		return text.toString();
	}

	@Nullable
	public static Note parseTextNoteFromXml(@NonNull String xmlNote) {
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser parser = factory.newPullParser();
			parser.setInput(new StringReader(xmlNote));
			int eventType = parser.getEventType();
			boolean done = false;
			Note note = null;
			while (eventType != XmlPullParser.END_DOCUMENT && !done) {
				String name;
				switch (eventType) {
					case XmlPullParser.START_DOCUMENT:
						break;
					case XmlPullParser.START_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase(XML_TAG_NOTE)) {
							note = new Note();
						} else if (note != null) {
							if (name.equalsIgnoreCase(XML_TAG_TITLE)) {
								note.setTitle(parser.nextText());
							} else if (name.equalsIgnoreCase(XML_TAG_BODY)) {
								note.setBody(parser.nextText());
							}
						}
						break;
					case XmlPullParser.END_TAG:
						name = parser.getName();
						if (name.equalsIgnoreCase(XML_TAG_NOTE) && note != null) {
							done = true;
						}
						break;
				}
				eventType = parser.next();
			}
			return note;
		} catch (Exception e) {
			LogUtils.e("PARSING_NOTE", e);
			return null;
		}
	}

	private static CharSequence trim(CharSequence text) {
		if (text.length() != 0) {
			while (text.charAt(text.length() - 1) == '\n') {
				text = text.subSequence(0, text.length() - 1);
			}
		}
		return text;
	}
}
